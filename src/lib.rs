use std::collections::HashMap;

use config::{Format, FileStoredFormat, Value, ValueKind};

use nickel_lang::match_sharedterm;
use nickel_lang::term::{Term, RichTerm, array::Array, record::RecordData};
use nickel_lang::program::Program;

static FILE_EXTENSIONS: &'static [&'static str] = &["ncl"];

#[derive(Clone, Debug)]
pub struct Nickel;

impl Format for Nickel {
    fn parse(
        &self,
        uri: Option<&String>,
        text: &str,
    ) -> Result<config::Map<String, Value>, Box<dyn std::error::Error + Send + Sync>> {
        let mut program = Program::new_from_source(
            text.as_bytes(),
            uri.map(|s| s.as_str()).unwrap_or(""),
        )?;

        let rt = program.eval_full()
            // Nickel errors contains Rc<T>, which isn't sync. So I'm just going to convert
            // the error message here and pass that along.
            //
            // I hope I can actually pretty print this at some point, that is todo
            .map_err(|err| format!("Failed to evaluate Nickel Program {err:?}"))?;

        if let Some(value) = convert_term(&rt) {
            if let ValueKind::Table(table) = value.kind {
                return Ok(table)
            }

            return Err(format!("Found value was not of table type: {value:?}").into());
        }

        Err(concat!(
            "Failed to convert Nickel evaluation to valid config value. ",
            "Handled terms include Null, Bool, Num, Str, Enum, Record, and Array.",
        ).into())
    }
}

impl FileStoredFormat for Nickel {
    fn file_extensions(&self) -> &'static [&'static str] {
        FILE_EXTENSIONS
    }
}

fn convert_num(num: f64) -> Value {
    if num.fract() == 0.0 {
        return Value::new(None, num as i64);
    }

    Value::new(None, num)
}

fn convert_map(record: &RecordData) -> Option<HashMap<String, Value>> {
    let record = record.fields
        .iter()
        .filter_map(|(k, v)| {
            let value = convert_term(v)?;

            Some((k.into_label(), value))
        })
        .collect::<HashMap<String, Value>>();

    if record.len() == 0 {
        return None;
    }

    Some(record)
}

fn convert_arr(array: &Array) -> Option<Vec<Value>> {
    let array = array.iter()
        .filter_map(|t| convert_term(t))
        .collect::<Vec<Value>>();

    if array.len() == 0 {
        return None;
    }

    Some(array)
}

fn convert_term(term: &RichTerm) -> Option<Value> {
    match_sharedterm!{ term.term.clone() /* term is an Rc<T>, so eh */, with {
        Term::Null => Some(Value::new(None, ValueKind::Nil)),
        Term::Bool(b) => Some(Value::new(None, b)),
        Term::Num(n) => Some(convert_num(n)),
        Term::Str(s) => Some(Value::new(None, s)),
        Term::Enum(i) => Some(Value::new(None, i.label())),
        Term::Record(r) => Some(Value::new(None, convert_map(&r)?)),
        Term::Array(a, _) => Some(Value::new(None, convert_arr(&a)?)),
    } else None }
}
