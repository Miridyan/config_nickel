// The file this is pulling from was taken from tweag/nickel-lang on github.
//
// These structures are just barebones enough to demonstrate successful use
// of serde in config.

use config::{Config, File};
use config_nickel::Nickel;

use serde::{Serialize, Deserialize};

#[derive(Debug, Deserialize, Serialize)]
struct KubernetesConfig {
    kind: Kind,

    #[serde(rename = "apiVersion")]
    api_version: String,
    metadata: Metadata,
    spec: KubernetesSpec,
}

#[derive(Debug, Deserialize, Serialize)]
enum Kind {
    ReplicationController,
    ReplicaSet,
    Pod,
}

#[derive(Debug, Deserialize, Serialize)]
struct Metadata {
    name: String,
    labels: Labels,
}

#[derive(Debug, Deserialize, Serialize)]
struct Labels {
    app: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct KubernetesSpec {
    replicas: u64,
    selector: Selector,
    template: Template,
}

#[derive(Debug, Deserialize, Serialize)]
struct Selector {
    app: App,

    #[serde(rename = "matchLabels")]
    match_labels: MatchLabels,
}

#[derive(Debug, Deserialize, Serialize)]
struct App {
    name: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct MatchLabels {
    app: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct Template {
    metadata: Metadata,
    spec: TemplateSpec,
}

#[derive(Debug, Deserialize, Serialize)]
struct TemplateSpec {
    containers: Vec<Container>,
}


#[derive(Debug, Deserialize, Serialize)]
struct Container {
    name: String,
    image: String,
    ports: Vec<Port>
}

#[derive(Debug, Deserialize, Serialize)]
struct Port {
    name: String,

    #[serde(rename = "containerPort")]
    container_port: u16,
}

fn main() {
    let config_file = Config::builder()
        .add_source(File::new("./examples/simple/record-contract.ncl", Nickel))
        .build()
        .expect("Failed to build config");

    let config = config_file
        .try_deserialize::<KubernetesConfig>()
        .expect("Failed to deserialize config");

    println!("{config:#?}");
}
